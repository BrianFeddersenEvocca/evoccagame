class EvoccaGamePawn extends UDKPawn;

var DynamicLightEnvironmentComponent LightEnvironment;

function float MyLittleFunction(float OurNumber, optional string RandomMessage)
{
	// Instantiate our little variable
	local float OurLittleVariable;
	local string OurLittleString; 

	OurLittleVariable = OurNumber;
	OurLittleString = RandomMessage;

	// Let's do some math
	OurLittleVariable = OurLittleVariable + 2.0; // Add two to our Variable
	OurLittleVariable = OurLittleVariable - 2.0; // Subtract two from our Variable
	OurLittleVariable = OurLittleVariable * 4.0; // Multiply our variable by four
	OurLittleVariable = OurLittleVariable / 4.0; // Divide our variable by four

	// Let's do some string work
	OurLittleString = OurLittleString $ " with extra!"; // Dollar sign is like the addition for strings (This is called, concatenation)
	OurLittleString = OurLittleString @ "Also with more!!!"; // At sign is like the dollar sign, but automatically adds in a space

	`log(OurLittleString);
	return OurLittleVariable; 
} 

simulated function PostBeginPlay()
{
	local float TestingFloat;

	super.PostBeginPlay();

	TestingFloat = MyLittleFunction(1.5, "Here be our random little message");
	`log("Testing Float: "$TestingFloat);
}

defaultproperties
{
	WalkingPct=+0.4
	CrouchedPct=+0.4
	BaseEyeHeight=38.0
	EyeHeight=38.0
	GroundSpeed=440.0
	AirSpeed=440.0
	WaterSpeed=220.0
	AccelRate=2048.0
	JumpZ=322.0
	CrouchHeight=29.0
	CrouchRadius=21.0
	WalkableFloorZ=0.78

	Components.Remove(Sprite)

	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
		bSynthesizeSHLight=TRUE
		bIsCharacterLightEnvironment=TRUE
		bUseBooleanEnvironmentShadowing=FALSE
	End Object
	Components.Add(MyLightEnvironment)
	LightEnvironment=MyLightEnvironment

	Begin Object Class=SkeletalMeshComponent Name=WPawnSkeletalMeshComponent
		//Your Mesh Properties
		SkeletalMesh=SkeletalMesh'CH_LIAM_Cathode.Mesh.SK_CH_LIAM_Cathode'
		AnimTreeTemplate=AnimTree'CH_AnimHuman_Tree.AT_CH_Human'
		PhysicsAsset=PhysicsAsset'CH_AnimCorrupt.Mesh.SK_CH_Corrupt_Male_Physics'
		AnimSets(0)=AnimSet'CH_AnimHuman.Anims.K_AnimHuman_BaseMale'
		Translation=(Z=8.0)
		Scale=1.075
		//General Mesh Properties
		bCacheAnimSequenceNodes=FALSE
		AlwaysLoadOnClient=true
		AlwaysLoadOnServer=true
		bOwnerNoSee=false
		CastShadow=true
		BlockRigidBody=TRUE
		bUpdateSkelWhenNotRendered=false
		bIgnoreControllersWhenNotRendered=TRUE
		bUpdateKinematicBonesFromAnimation=true
		bCastDynamicShadow=true
		RBChannel=RBCC_Untitled3
		RBCollideWithChannels=(Untitled3=true)
		LightEnvironment=MyLightEnvironment
		bOverrideAttachmentOwnerVisibility=true
		bAcceptsDynamicDecals=FALSE
		bHasPhysicsAssetInstance=true
		TickGroup=TG_PreAsyncWork
		MinDistFactorForKinematicUpdate=0.2
		bChartDistanceFactor=true
		RBDominanceGroup=20
		bUseOnePassLightingOnTranslucency=TRUE
		bPerBoneMotionBlur=true
	End Object
	Mesh=WPawnSkeletalMeshComponent
	Components.Add(WPawnSkeletalMeshComponent)

	Begin Object Name=CollisionCylinder
		CollisionRadius=+0021.000000
		CollisionHeight=+0044.000000
	End Object
	CylinderComponent=CollisionCylinder
}